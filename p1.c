#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

//Definiciones
#define N 5
#define IZQUIERDA(x) (((x)>0) ? (x)-1 : N-1)
#define DERECHA(x)   (((x)+1)%N)
#define VECES 1


//Declaracion de valores
typedef enum {PENSANDO, HAMBRIENTO,
COMIENDO} status;
status estado[N];
pthread_mutex_t mutex, s[N];


//Declaracion de funciones
void *filosofo(void *p);
void Pensar(int i);
void Comer(int i);
void CogerTenedores(int i);
void SoltarTenedores(int i);
void Comprobar(int i);



int main()
{
	extern status estado[N];
	extern pthread_mutex_t mutex, s[N];
    pthread_t filosofos[N];
    int stat, i,v[N];


    //Inicialización de variables
	pthread_mutex_init(&mutex, NULL);

	for(i=0; i < N; i++){
		pthread_mutex_init(&s[i], NULL);
		pthread_mutex_lock(&s[i]);

	}
	
	for(i=0; i < N; i++){	
		estado[i] = PENSANDO;
	}
	
    
    srand (time(NULL));

  	
    // Create NHILOS threads
    for (i = 1; i <= N; i++) {
    	v[i] = i;
		if ((stat = pthread_create(&filosofos[i], NULL, filosofo,(void *) &v[i])))
	    	exit(stat);
		
    }

    
    // Wait threads
    for (i = 1; i <= N; i++) {
		pthread_join(filosofos[i],NULL);
		
    }


    return 0;
}



void *filosofo(void *p)
{

	int *id;

	id=(int *) p;

   for (int i = 0; i < VECES; ++i)
   {
   		Pensar(*id);
		CogerTenedores(*id);
		Comer(*id);
		SoltarTenedores(*id);
   }
	
	pthread_exit(NULL);
}


void Pensar(int i)
{
	int rand=random() % 10;
	printf("El filosofo %i PIENSA durante %i segundos.\n", i, rand);
	sleep (rand);
}

void Comer(int i)
{
	int rand=random() % 10;
	printf("El filosofo %i COME durante %i segundos.\n", i, rand);
	sleep (rand);
}


void CogerTenedores(int i)
{
    extern pthread_mutex_t mutex, s[N];
    extern status estado[N];

    pthread_mutex_lock(&mutex);
    estado[i] = HAMBRIENTO;
    Comprobar(i);
    printf("El filosofo %i, COGE TENEDORES\n", i);
    pthread_mutex_unlock(&mutex);
    pthread_mutex_lock(&s[i]);
}


void SoltarTenedores(int i)
{
    extern pthread_mutex_t mutex, s[N];
    extern status estado[N];

    pthread_mutex_lock(&mutex);
    estado[i] = PENSANDO;
    Comprobar(IZQUIERDA(i));
    Comprobar(DERECHA(i));
    printf("El filosofo %i, SOLTAR TENEDORES\n", i);
    pthread_mutex_unlock(&mutex);
}


void Comprobar(int i)
{
    extern pthread_mutex_t  s[N];
    extern status estado[N];

    if (estado[i] == HAMBRIENTO && estado[IZQUIERDA(i)] != COMIENDO &&
	estado[DERECHA(i)] != COMIENDO) {
	estado[i] = COMIENDO;
	pthread_mutex_unlock(&s[i]);
    }
}